﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

	
namespace ClassHomework
{
  public class Smartphone
  {
    public string Make {get; set;}
    public string Model {get; set;}
    public Camera FrontCam { get; set; }
    public Camera BackCam { get; set; }
    public string ScreenSize { get; set; }
    public string ScreenRsltn { get; set; }
    public string Dimensions { get; set; }
    public string BattCap { get; set; }
    public string Processor { get; set; }
    public string StorSpace { get; set; }
  }
}
