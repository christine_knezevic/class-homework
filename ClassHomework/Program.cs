﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassHomework
{
  class Program
  {
    static void Main(string[] args)
    {
      Smartphone A = new Smartphone();
      
      //now, FrontCam and BackCam are each their own Camera class
      //intialize them
      A.FrontCam = new Camera();
      A.BackCam = new Camera();

      A.Make = "Samsung";
      A.Model = "Galaxy S5";
      A.ScreenSize = "5.1 inches";
      A.ScreenRsltn = "1080 x 1920 pixels";
     
      //here's how you set the FrontCam and BackCam properties
      A.FrontCam.MegPix = 16;
      A.BackCam.MegPix = 2;

      A.FrontCam.HasFlash = false;
      A.BackCam.HasFlash = true;
      
      A.Dimensions = "5.59 x 2.85 x 0.32 in";
      A.BattCap = "21 h";
      A.Processor = "Quad-core 2.5 GHz Krait 400";
      A.StorSpace = "16/32 GB, 2 GB RAM";
      
      //this is where it outputs to the screen
      Console.WriteLine("The phone is a {0}", A.Make);
      Console.WriteLine("It is a {0} Model", A.Model);
      Console.WriteLine("It has a {0} screen size", A.ScreenSize);
      Console.WriteLine("The screen resolution is {0}", A.ScreenRsltn);
      Console.WriteLine("The front camera has {0} megapixels", A.FrontCam.MegPix);
      Console.WriteLine("The back camera has {0} megapixels", A.BackCam.MegPix);
      Console.WriteLine("It is {0}", A.Dimensions);
      Console.WriteLine("It has {0} battery life", A.BattCap);
      Console.WriteLine("It has a {0} processor", A.Processor);
      Console.WriteLine("It has {0} storage space", A.StorSpace);
      Console.Read();
    }
  }
}
