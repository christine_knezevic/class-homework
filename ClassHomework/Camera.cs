﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassHomework
{
  public class Camera
  {
    //typically, this is what you'll see
    public int MegPix { get; set; }
    
    //this is the equivalent of the above statement
    //public int MegPix;

    public bool? HasFlash { get; set; }
  }
}
